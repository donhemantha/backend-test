import React from "react";
import { render } from "react-dom";

const styles = {
  fontFamily: "sans-serif",
  textAlign: "center"
};

class App extends React.Component {
  render() {
    const data = [
      {
        "product": "1932 Model A Ford J-Coupe",
        "product_line": "Vintage Cars",
        "unit_price": "108.06",
        "qty": 25,
        "line_total": "2701.50"
      },
      {
        "product": "1928 Mercedes-Benz SSK",
        "product_line": "Vintage Cars",
        "unit_price": "167.06",
        "qty": 26,
        "line_total": "4343.56"
      },
      {
        "product": "1939 Chevrolet Deluxe Coupe",
        "product_line": "Vintage Cars",
        "unit_price": "32.53",
        "qty": 45,
        "line_total": "1463.85"
      },
      {
        "product": "1938 Cadillac V-16 Presidential Limousine",
        "product_line": "Vintage Cars",
        "unit_price": "44.35",
        "qty": 46,
        "line_total": "2040.10"
      }
    ];

    return (
      <div>
      {data.map(function(d, idx){
         return (<li key={idx}>{d.product}</li>)
       })}
      </div>
    );
  }
}

render(<App />, document.getElementById("myorders"));
