import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {useState, useEffect} from 'react';
import axios from 'axios';
import { Route, useParams } from "react-router-dom";
import { withRouter } from "react-router";


function ViewOrder(props) {
        
        const [data, setData] = useState([]);
        const [customerData, setCustomer] = useState("");
        const [orderDetailData, setOrderDetails] = useState("");
        
        useEffect(()=>{
            getData();
        }, [])

        async function getData(){
            let result = await fetch("http://127.0.0.1:8000/orders/10101");
            result = await result.json();
            setData(result);    
            setOrderDetails(result.order_details);       
            setCustomer(result.customer);  
        }        
            
         
            console.warn(orderDetailData);
            // console.warn(initState);

  
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-8">
                        <div className="card">
                            <div className="card-header"><h3>Order Information</h3></div>
                            <div className="ordData">Order Number : {data.order_id} </div>
                            <div className="ordData">Order Date : {data.order_date} </div>
                            <div className="ordData">Order Statu : {data.status} </div>
                        </div>

                        <div className="card">
                            <div className="card-header"><h3>Product Oredered </h3></div> 

                            <div>  
                            
                            </div>

                        </div>

                        <div className="card">
                            <div className="card-header">Billed Amount : {data.bill_amount}  USD </div>  
                        </div>

                    </div>
                    <div className="col-md-4">
                            <h3>Customer information </h3> 
                            <div>Name : { customerData.first_name} { customerData.last_name} </div>
                            <div>Phone : {customerData.phone} </div>
                            <div>Country : {customerData.country_code} </div>
                    </div>
                </div>
            </div>
        );   
    
}

// export default ViewOrder
export default withRouter(ViewOrder);

if (document.getElementById('myorders')) {
    ReactDOM.render(<ViewOrder />, document.getElementById('myorders'));
}
