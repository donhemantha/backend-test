<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Backend Testing - View Order Information </title>

        <link href="/css/app.css" rel="stylesheet" />
        <link href="/css/recipt.css" rel="stylesheet" />
        
    </head>
    <body>
        <h3> &nbsp; </h3>
        <div id="myorders"></div>            
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
