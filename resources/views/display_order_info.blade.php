<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Order Information</title>
</head>

<body>
    <div class="container">

    <?php if($order_data == "NoData"){?>    
        <div class="row"> No Data Available </div>
    <?php }else{?>

    <div class="row">
            <div class="col-sm">
                <div className="card">
                    <div className="card-header"><h3>Customer information </h3> </div>
                    <div className="ordData">Customer Name : {{$order_data['customer']['first_name']}} {{$order_data['customer']['last_name']}}  </div>
                    <div className="ordData">Phone : {{$order_data['customer']['phone']}} </div>
                    <div className="ordData">Country : {{$order_data['customer']['country_code']}} </div>
                </div>
            </div>
            <div class="col-sm">
                <div className="card-header"><h3>Product Oredered</h3></div>
                    <div className="card">

                        <?php
                        if(count($order_data['order_details']) > 0){
                            ?>
                            <table class="table">
  
                            <tbody>
                                
                            <?php
                            foreach($order_data['order_details'] as $pro){
                             ?>   
                                <tr>
                                    <td colspan="3"> 
                                      Product :  {{$pro['product']}}  <br/>
                                      Product line :  {{$pro['product_line']}} <br/>
                                      Price :  {{$pro['unit_price']}} <br/>
                                      Qty :  {{$pro['qty']}} <br/>
                                      Line Total :  {{$pro['line_total']}} <br/>
                                    
                                    </td>
                                </tr> 
                                <?php
                            }
                        }else{
                            echo("no data available");
                        }
                        ?>
                            </tbody>
                            </table>


                    </div>
            </div>
            <div class="col-sm">
                <div className="card">
                    <div className="card-header"><h3>Order Information</h3></div>
                    <div className="ordData">Order Number : {{$order_data['order_id']}} </div>
                    <div className="ordData">Order Date : {{$order_data['order_date']}} </div>
                    <div className="ordData">Order Staus : {{$order_data['status']}} </div>
                    <div className="ordData">Bill Amount : {{$order_data['bill_amount']}} USD </div>
                </div>
            </div>
        </div>

    <?php } ?>

    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>