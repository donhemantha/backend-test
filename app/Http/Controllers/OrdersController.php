<?php

namespace App\Http\Controllers;

use Illuminate\Http\Requests;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;

use App\Order;
use App\Orderdetail;
use App\Product;
use App\Productline;
use App\Customer;
use App\Payment;
use DB;



class OrdersController extends Controller
{

    /**
     * fetch order details
     * @param $id
     * @return array
     */

    public function fetchOrderData($id)
    {
        // your logic goes here

        //print("Order inofrmation" . $id);
        // $order_data = [];

        $order_result = Order::where('orderNumber', $id)->where('status', 'Shipped')->first();    
        
       

        $order_data['order_id'] = $order_result['orderNumber'];
        $order_data['order_date'] = $order_result['orderDate'];
        $order_data['status'] = $order_result['status'];       

        $orderdetail_result = Orderdetail::where('orderNumber', $order_data['order_id'])->get();        
        $toral_cost = 0;
        foreach($orderdetail_result as $key => $ord_data){          
           
            $product = Product::where('productCode', '=', $ord_data['productCode'])->first();  
                                 
            $orderdetail_data[$key]['product'] = $product['productName'];

            if(!$product['productLine']){
                 $orderdetail_data[$key]['product_line'] = "null";    
            }else{
                $orderdetail_data[$key]['product_line'] = $product['productLine'];    
            }
                   
            $orderdetail_data[$key]['unit_price'] = $ord_data['priceEach'];
            $orderdetail_data[$key]['qty'] = $ord_data['quantityOrdered'];
            $orderdetail_data[$key]['line_total'] = number_format((float)($ord_data['priceEach'] * $ord_data['quantityOrdered'] ), 2, '.', ''); ;

            $toral_cost = $toral_cost + $orderdetail_data[$key]['line_total'] ;
           
        }

        $order_data['order_details'] = $orderdetail_data;

        $billed_result = Payment::where('customerNumber', $order_result['customerNumber'])->get();   

        if(count($billed_result)>= 0){
            foreach($billed_result as $billed){  
                $myBilled = $billed['amount'] ; 
                if("$myBilled" == "$toral_cost"){
                  $toral_cost  =  $billed['amount'];
                    break;
                }
            }
        }

        $order_data['bill_amount'] = $toral_cost;

        $customer_result = Customer::where('customerNumber', $order_result['customerNumber'])->first();   

        
        $customer_data['first_name'] = $customer_result['contactFirstName'];
        $customer_data['last_name'] = $customer_result['contactLastName'];
        $customer_data['phone'] = $customer_result['phone'];       
        $customer_data['country_code'] = $customer_result['country'];       

        $order_data['customer'] = $customer_data;
        
        // print_r($customer_result);
        
        // print("<pre>");
        // print_r($order_data);
        // print("</pre>");

        // $myOrder = json_encode($order_data, JSON_PRETTY_PRINT);
        // echo $myOrder;

        return $order_data;

    }


    public function display(Request $req, $id){


        $order_data = [];
        $order_result = Order::where('orderNumber', $id)->where('status', 'Shipped')->first();   

        
        
        if($order_result){  

            $order_data['order_id'] = $order_result['orderNumber'];
            $order_data['order_date'] = $order_result['orderDate'];
            $order_data['status'] = $order_result['status'];       

            $orderdetail_result = Orderdetail::where('orderNumber', $order_data['order_id'])->get();        
            $toral_cost = 0;

            foreach($orderdetail_result as $key => $ord_data){          
            
                $product = Product::where('productCode', '=', $ord_data['productCode'])->first();  
                                    
                $orderdetail_data[$key]['product'] = $product['productName'];

                if(!$product['productLine']){
                    $orderdetail_data[$key]['product_line'] = "null";    
                }else{
                    $orderdetail_data[$key]['product_line'] = $product['productLine'];    
                }
                    
                $orderdetail_data[$key]['unit_price'] = $ord_data['priceEach'];
                $orderdetail_data[$key]['qty'] = $ord_data['quantityOrdered'];
                $orderdetail_data[$key]['line_total'] = number_format((float)($ord_data['priceEach'] * $ord_data['quantityOrdered'] ), 2, '.', ''); ;

                $toral_cost = $toral_cost + $orderdetail_data[$key]['line_total'] ;
            
            }

            $order_data['order_details'] = $orderdetail_data;

            $billed_result = Payment::where('customerNumber', $order_result['customerNumber'])->get();   

            if(count($billed_result)>= 0){
                foreach($billed_result as $billed){  
                    $myBilled = $billed['amount'] ; 
                    if("$myBilled" == "$toral_cost"){
                    $toral_cost  =  $billed['amount'];
                        break;
                    }
                }
            }

            $order_data['bill_amount'] = $toral_cost;

            $customer_result = Customer::where('customerNumber', $order_result['customerNumber'])->first();   

            
            $customer_data['first_name'] = $customer_result['contactFirstName'];
            $customer_data['last_name'] = $customer_result['contactLastName'];
            $customer_data['phone'] = $customer_result['phone'];       
            $customer_data['country_code'] = $customer_result['country'];       

            $order_data['customer'] = $customer_data;
            

            // print_r($order_data);
            // exit;
            // return view("display_order_info", compact("order_data"));

            // return $order_data;

            return view('display_order_info', ['order_data' => $order_data]);

        }else{

            
            $order_data = "NoData";
            return view('display_order_info', ['order_data' => $order_data]);
        }
        
    }
}
