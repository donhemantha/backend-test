<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('order_info');
// });

Route::view('/view_order/{id}', 'order_info');

Route::get('display_order/{id}', 'OrdersController@display');   

Route::get('orders/{id}', 'OrdersController@fetchOrderData');   
